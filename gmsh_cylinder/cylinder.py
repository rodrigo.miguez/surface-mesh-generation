#!/usr/bin/env python3

# Import libraries
import gmsh
import math
import sys
import os
import logging
import toml

# Set up logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(levelname)s: %(message)s')
console_handler = logging.StreamHandler()
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)

# TODO: cleanup/refactoring

# Check the command-line arguments
no_inputs = 1

if len(sys.argv) < 2:
    logger.error("No command-line arguments provided. For more information run: cylinder.py --help")
    sys.exit(1)

elif sys.argv[1] == "--help":
    logger.info(f'''This is a Python script for generating a 3D mesh of a cylindrical or conical geometry
      with a nozzle cavity using the Gmsh library. The script takes {no_inputs} input parameter
      from the user via command line arguments.

      Usage: python template.py <arg1>, where:
      <arg1> -> input file name (string)

      The input file must contain the following variables:
      <dir_name> -> output directory name (string), for most cases should be kept as "out/"
      <nozzle_origin> -> origin coordinates of the nozzle (array of floats, e.g. [0.0, 0.0, 0.0])
      <axis> -> axis direction (0: x, 1: y, 2: z) for the cylinder's axis (int)
      <radius> -> radius for each section, including initial and final radius (array of floats)
      <lengths> -> distance from origin for each section, including initial and final distances
                   (array of floats). First element will always be 0.0
      <cut_radius> -> radius of the nozzle cutout (float)
      <save_small_cylinder> -> define section number(s) up to which a smaller cylinder will be
                               saved (array of ints with len(save_small_cylinder) ranging from 0 to no_sections - 1),
                               e.g. if save_small_cylinder = [2] then it will additionally save a cylinder
                               formed by sections 1 and 2.
      <mesh_size> -> mesh size (float)

      So far the script can only handle axis-aligned geometries with constant mesh size throughout
      the whole geometry.

      For information on how to generate a simple geometry run: cylinder.py --test''')
    sys.exit(1)

elif sys.argv[1] == "--test":
    logger.info(f'''Run example case 1, input.toml file content:
      dir_name = "out/"

      nozzle_origin = [1.0, 1.0, 2.0]

      axis = 1

      radius = [1.0, 3.0]

      lengths = [0.0, 5.0]

      cut_radius = 0.5

      save_small_cylinder = []

      mesh_size = 0.1
      ''')

    logger.info(f'''Run example case 2, input.toml file content:
      dir_name = "out/"

      nozzle_origin = [0.0, 0.0, 0.0]

      axis = 0

      radius = [1.0, 1.0, 1.25, 2.0, 2.5, 2.0, 2.0]

      lengths = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0]

      cut_radius = 0.5

      save_small_cylinder = [2, 5]

      mesh_size = 0.1
      ''')
    sys.exit(1)

elif len(sys.argv) < no_inputs:
    logger.error(f"{no_inputs} command-line arguments must be provided. For more information run: cylinder.py --help")
    sys.exit(1)

# FUNCTIONS
# Read toml file
def readTomlFile(filename):
    with open(filename, 'r') as f:
        try:
            data = toml.load(f)
        except ValueError:
            logger.error(f"loading {filename}: {ValueError.args[0]}. For more information run: template.py --help")
            sys.exit(1)
    return data

# Read toml data
def readTomlData(data, data_name, data_type):
    # Check if data_name can be found at the input file
    try:
        variable_data = data[data_name]
    except:
        logger.error(f"Variable {data_name} doesn't exist in the input file. For more information run: template.py --help")
        sys.exit(1)

    # Check if the data_type matches
    if isinstance(variable_data, list):
        for i in variable_data:
            if not isinstance(i, data_type):
                logger.error(f"Variable {data_name} has a element: {i}, which is not of the expected type. For more information run: template.py --help")
                sys.exit(1)
    else:
        if not isinstance(variable_data, data_type):
            logger.error(f"Variable {data_name} is not of the expected type. For more information run: template.py --help")
            sys.exit(1)
    return variable_data

# Delete previous files in output directory
def cleanDir(dir_name):
    cmd = "rm " + dir_name + "*"
    os.system(cmd)

# Set mesh size and generate the mesh
def genMesh(mesh_size):
    gmsh.model.mesh.setSize(gmsh.model.getEntities(0), mesh_size)

    try:
        gmsh.model.mesh.generate(2)
    except:
        logger.error("Mesh generation failed!")
        gmsh.finalize()
        sys.exit(1)

# Write mesh in .stl format
def writeStl(filename):
    try:
        gmsh.write(filename + ".stl")
    except:
        logger.error("Failed to save mesh to file!")
        gmsh.finalize()
        sys.exit(1)

    print(f"Mesh saved to {filename}.stl")

def updateTags(tags):
    if tags: # if not empty
        tags.append(len(tags) + 1)
    else: # if empty
        tags.append(1)

# Generate circle with cutout geometry
def genCutoutCircle(radius, radius_cut, axis_dir, nozzle_origin, axis, tags):
    # Define Circle and cutout
    c = gmsh.model.occ.addCircle(nozzle_origin[0], nozzle_origin[1], nozzle_origin[2], radius)
    c_cut = gmsh.model.occ.addCircle(nozzle_origin[0], nozzle_origin[1], nozzle_origin[2], radius_cut)

    # Rotate circles if it's not in z direction
    if axis != 2:
        # Rotate around another axis direction, swap x and y directions
        gmsh.model.occ.rotate([(1, c)], nozzle_origin[0], nozzle_origin[1], nozzle_origin[2], \
                              axis_dir[1], axis_dir[0], axis_dir[2], math.pi / 2)

        gmsh.model.occ.rotate([(1, c_cut)], nozzle_origin[0], nozzle_origin[1], nozzle_origin[2], \
                              axis_dir[1], axis_dir[0], axis_dir[2], math.pi / 2)

    # Create curve loops
    cl = gmsh.model.occ.addCurveLoop([c])
    cl_cut = gmsh.model.occ.addCurveLoop([c_cut])

    # Create plane surface
    gmsh.model.occ.addPlaneSurface([cl, cl_cut])

    updateTags(tags)

# Generate cylinder geometry
def genCylinder(min_radius, max_radius, init_len, end_len, nozzle_origin, axis, tags):
    # Define Points
    p1 = 0
    p2 = 0
    if axis == 0:
        p1 = gmsh.model.occ.addPoint(init_len, nozzle_origin[1] + min_radius, nozzle_origin[2])
        p2 = gmsh.model.occ.addPoint(end_len, nozzle_origin[1] + max_radius, nozzle_origin[2])
    elif axis == 1:
        p1 = gmsh.model.occ.addPoint(nozzle_origin[0], init_len, nozzle_origin[2] + min_radius)
        p2 = gmsh.model.occ.addPoint(nozzle_origin[0], end_len, nozzle_origin[2] + max_radius)
    elif axis == 2:
        p1 = gmsh.model.occ.addPoint(nozzle_origin[0] + min_radius, nozzle_origin[1], init_len)
        p2 = gmsh.model.occ.addPoint(nozzle_origin[0] + max_radius, nozzle_origin[1], end_len)
    else:
        logger.error("Axis direction must be 0, 1, or 2.")
        sys.exit(1)

    # Create line
    ln = gmsh.model.occ.addLine(p1, p2)

    # Create cylinder revolution from line
    gmsh.model.occ.revolve([(1, ln)], nozzle_origin[0], nozzle_origin[1], nozzle_origin[2], \
                                      axis_dir[0], axis_dir[1], axis_dir[2], 2 * math.pi)

    updateTags(tags)

# Generate circle geometry
def genCircle(radius, nozzle_distance, axis_dir, nozzle_origin, axis, tags):
    # Create x, y, z coordinates
    x, y, z = [nozzle_origin[i] + axis_dir[i] * nozzle_distance for i in range(3)]

    # Define Circle (always defined in x-y plane)
    c = gmsh.model.occ.addCircle(x, y, z, radius)

    # Rotate circles if it's not in z direction
    if axis != 2:
        # Rotate around another axis direction, swap x and y directions
        gmsh.model.occ.rotate([(1, c)], x, y, z, axis_dir[1], axis_dir[0], axis_dir[2], math.pi / 2)

    # Create curve loop
    cl = gmsh.model.occ.addCurveLoop([c])

    # Create plane surface
    gmsh.model.occ.addPlaneSurface([cl])

    updateTags(tags)

# INITIALIZATION
# Initialize gmsh and set options
gmsh.initialize()
gmsh.option.setNumber("General.Terminal", 1)

# Clear all models and create a new one
gmsh.clear()
gmsh.model.add("mesh")

# Read Toml
input_file_name = sys.argv[1]
data = readTomlFile(input_file_name)

# Initialize variables and prepare data
# Get property dir_name
dir_name = readTomlData(data, 'dir_name', str)

# Get property nozzle_origin
nozzle_origin = readTomlData(data, 'nozzle_origin', float)

# Get property axis
axis = readTomlData(data, 'axis', int)
if not axis in [0, 1, 2]:
    logger.error(f"axis is {axis} and should be 0, 1 or 2. For more information run: template.py --help")
    sys.exit(1)
axisDirMap = {0: [1.0, 0.0, 0.0], 1: [0.0, 1.0, 0.0], 2: [0.0, 0.0, 1.0]}
axis_dir = axisDirMap.get(axis, [0.0] * 3)

# Get property radius
radius = readTomlData(data, 'radius', float)
for i in range(len(radius)):
    if radius[i] <= 0.0:
        logger.error(f"Element {i} of the array radius is {radius[i]} and should be > 0.0. For more information run: template.py --help")
        sys.exit(1)

# Get property cut_radius
cut_radius = readTomlData(data, 'cut_radius', float)
if cut_radius <= 0.0:
    logger.error(f"cut_radius is {cut_radius} and should be > 0.0. For more information run: template.py --help")
    sys.exit(1)
if radius[0] <= cut_radius:
    logger.error("cutout_radius must be smaller than init_radius.")
    sys.exit(1)

# Get property lengths
lengths = readTomlData(data, 'lengths', float)
if lengths[0] != 0.0:
    logger.error(f"Element {i} of the array lengths is {lengths[i]} and should be 0.0. For more information run: template.py --help")
    sys.exit(1)
final_distance = lengths[len(lengths) - 1]
for i in range(len(lengths)):
    lengths[i] += nozzle_origin[axis]

# Abort if the arrays radius and lengths differ in size
if len(radius) != len(lengths):
    logger.error(f"radius and lenths are of different sizes. For more information run: template.py --help")
    sys.exit(1)
# Get property no_sections
no_sections = len(radius) - 1
if no_sections < 1:
    logger.error(f"no_sections is {no_sections} and should be >= 1. For more information run: template.py --help")
    sys.exit(1)

# Get property save_small_cylinder
save_small_cylinder = readTomlData(data, 'save_small_cylinder', int)
if len(save_small_cylinder) > no_sections - 1:
    logger.error(f"Length of array save_small_cylinder is {len(save_small_cylinder)} and should be at most equal to {no_sections - 1}. For more information run: template.py --help")
    sys.exit(1)
for i in range(len(save_small_cylinder)):
   if save_small_cylinder[i] < 1:
       logger.error(f"Element {i} of the array save_small_cylinder is {save_small_cylinder[i]} and should be >= 1. For more information run: template.py --help")
       sys.exit(1)
   if save_small_cylinder[i] > no_sections - 1:
       logger.error(f"Element {i} of the array save_small_cylinder is {save_small_cylinder[i]} and should be <= {no_sections - 1}. For more information run: template.py --help")
       sys.exit(1)

# Get property mesh_size
mesh_size = readTomlData(data, 'mesh_size', float)
if mesh_size <= 0.0:
    logger.error(f"mesh_size is {mesh_size} and should be > 0.0. For more information run: template.py --help")
    sys.exit(1)

# Create empty array of tags so it can be used to create physical groups
tags = []

# GEOMETRIES
# Create cylinder or cone
for i in range(no_sections):
    genCylinder(radius[i], radius[i + 1], lengths[i], lengths[i + 1], nozzle_origin, axis, tags)

# Create circle with nozzle cutout
genCutoutCircle(radius[0], cut_radius, axis_dir, nozzle_origin, axis, tags)

# Create circle at the end of the cone or cylinder
genCircle(radius[no_sections], final_distance, axis_dir, nozzle_origin, axis, tags)

# Create an additional circle to enclose a smaller cone of minimum 2 sections
if no_sections > 1:
    for i in range(len(save_small_cylinder)):
        genCircle(radius[save_small_cylinder[i]], lengths[save_small_cylinder[i]], axis_dir, nozzle_origin, axis, tags)

# Synchronize cad entities with gmsh model
gmsh.model.occ.synchronize()

# MESH
# TODO: assign different mesh size for each surface
genMesh(mesh_size)

# PHYSICAL GROUPS AND WRITE
# Separate files (exploit the fact that by default, when physical groups are defined,
#                 Gmsh only saves the entities that belong to physical groups)
# Clean output folder
cleanDir(dir_name)

# Save whole geometry
if no_sections > 2:
    group_name = "all"
    for i in range(no_sections + 2):
        tag = tags[i]
        gmsh.model.addPhysicalGroup(2, [tag], tag * 100, "tag")
    writeStl(dir_name + "mesh_" + group_name)
else:
    group_name = "all"
    writeStl(dir_name + "mesh_" + group_name)

# Save Cylinder
group_name="Cylinder"
for i in range(no_sections):
    gmsh.model.removePhysicalGroups()
    tag = tags[i]
    gmsh.model.addPhysicalGroup(2, [tag], tag * 100, "Cylinder")
    writeStl(dir_name + "mesh_" + group_name + str(tag))

# Save Cutout circle
gmsh.model.removePhysicalGroups()
group_name="CircleCut"
tag = tags[no_sections]
gmsh.model.addPhysicalGroup(2, [tag], tag * 100, "CutoutCircle")
writeStl(dir_name + "mesh_" + group_name)

# Save Circle
gmsh.model.removePhysicalGroups()
group_name="Circle"
tag = tags[no_sections + 1]
gmsh.model.addPhysicalGroup(2, [tag], tag * 100, "Circle")
writeStl(dir_name + "mesh_" + group_name)

# Save additional cylinder geometries
if no_sections > 1:
    for i in range(len(save_small_cylinder)):
        # Section circle
        gmsh.model.removePhysicalGroups()
        group_name="CircleSection_" + str(save_small_cylinder[i])
        tag = tags[no_sections + 2 + i]
        gmsh.model.addPhysicalGroup(2, [tag], tag * 100, "CircleSection")
        writeStl(dir_name + "mesh_" + group_name)

        # Cylinder formed until the choosen section
        gmsh.model.removePhysicalGroups()
        group_name="smallCylinder_" + str(save_small_cylinder[i])
        small_cone_tags = [tags[j] for j in range(save_small_cylinder[i])] + [tags[no_sections]] + [tags[no_sections + 2 + i]]
        for j in range(len(small_cone_tags)):
            tag = small_cone_tags[j]
            gmsh.model.addPhysicalGroup(2, [tag], tag * 100, "tag")
        writeStl(dir_name + "mesh_" + group_name)

# Finalize Gmsh API
gmsh.finalize()
